<div align="center">
    <h2 align="center">Desafio Vizir - Frontend</h3>
</div>

# Índice
- [Tecnologias Utilizadas](#tecnologias-utilizadas)
- [Overview da Aplicação](#overview)
- [Estrutura da Aplicação](#estrutura)
- [Iniciando a Aplicação](#executando)

<a id="tecnologias-utilizadas"></a>

## :wrench: Tecnologias Utilizadas

- [ReactJS](https://pt-br.reactjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [React Router](https://reactrouter.com/web/guides/quick-start)
- [React Hooks](https://pt-br.reactjs.org/docs/hooks-intro.html)
- [Material UI -Tables](https://material-ui.com/pt/components/tables/)

<a id="overview"></a>

## :mag: Overview da Aplicação

Como podemos ver abaixo. A inteface Web tem como objetivo realizar o calculo de uma chamada telefonica e apresentar os beneficios de se utilizar o produto **FaleMais**

<h1 align="center">
    <img alt="Web" src="files/sample.gif" width="900px">
</h1>

<a id="estrutura"></a>

## :deciduous_tree: Estrutura da Aplicação

```
├── public
├── src
│   ├── assets
│   │   ├── images
│   │   └── styles
│   ├── components
│   │   ├── Header
│   │   ├── Input
│   │   └── Table
│   ├── pages
│   │   ├── ChargeCalc
│   │   └── Landing
│   ├── services
│   ├── App.tsx
│   ├── index.tsx
│   └── react-app-env.d.ts
├── package.json
├── tsconfig.json
├── yarn.lock
└── README.md
```

<a id="executando"></a>

## :rocket: Iniciando a aplicação

- Acesse a pasta do projeto
- Na raiz do projeto execute o comando abaixo para instalação das dependências
```sh
  $ yarn install
```

- Inicie a aplicação
```sh
  $ yarn start
```