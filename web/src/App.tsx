import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import './assets/styles/global.css'

import Landing from './pages/Landing';
import ChargeCalc from './pages/ChargeCalc';

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Route path="/" exact component={Landing} />
      <Route path="/charge" exact component={ChargeCalc} />
    </BrowserRouter>
  )
}
export default App;
