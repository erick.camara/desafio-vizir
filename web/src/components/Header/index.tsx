import React from 'react';
import { Link } from 'react-router-dom';

import logo from '../../assets/images/logo.png';
import back from '../../assets/images/back.png';

import './styles.css'

interface PageHeaderProps {
  title: string;
  description?: string;
}

const PageHeader: React.FC<PageHeaderProps> = (props) => {
  return (
    <header className="header">
      <div className="top-bar-container">
        <Link to="/">
          <img src={back} alt="Voltar"/>
        </Link>
        <img src={logo} alt="FaleMais"/>
      </div>

      <div className="header-content">
        <strong>{props.title}</strong>
        { props.description && <p>{props.description}</p>}
        {props.children}
      </div>
    </header>
  )
}
export default PageHeader;