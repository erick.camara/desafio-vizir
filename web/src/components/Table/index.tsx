import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import './styles.css';

interface TablesProps {
  dddFrom: number;
  dddTo: number;
  plain: string;
  callTime: number;
  withPlain: number;
  withoutPlain: number;
}

const ChargeTable: React.FC<TablesProps> = (data) => {
  return (
    <Table>
      <TableHead className="table-header">
        <TableRow>
          <TableCell id="header-cell">DDD Origem</TableCell>
          <TableCell id="header-cell">DDD Destino</TableCell>
          <TableCell id="header-cell">Plano</TableCell>
          <TableCell id="header-cell">Duração da chamada</TableCell>
          <TableCell id="header-cell">Com Fale Mais</TableCell>
          <TableCell id="header-cell">Sem Fale Mais</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow key={data.dddFrom}>
          <TableCell id="table-content-cell">{data.dddFrom}</TableCell>
          <TableCell id="table-content-cell">{data.dddTo}</TableCell>
          <TableCell id="table-content-cell">{data.plain}</TableCell>
          <TableCell id="table-content-cell">{data.callTime}</TableCell>
          <TableCell id="table-content-cell">{data.withPlain >= 0 ? 'R$ ' + data.withPlain.toFixed(2) : data.withPlain}</TableCell>
          <TableCell id="table-content-cell">{data.withoutPlain >= 0 ? 'R$ ' + data.withoutPlain.toFixed(2) : data.withoutPlain}</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  );
}
export default ChargeTable;