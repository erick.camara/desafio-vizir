import React, { useState, useEffect, FormEvent } from 'react';

import './styles.css';
import Header from '../../components/Header';
import Input from '../../components/Input';
import ChargeTable from '../../components/Table';
import api from '../../services/api';

interface FromResponse {
  dddFrom: number,
}

interface ToResponse {
  dddTo: number,
}

interface PlansResponse {
  id: number,
  name: string;
}

interface CostResponse {
  from: number,
  to: number,
  time: number,
  plain: string,
  withPlain:number,
  withoutPlain:number
}

const ChargeCalc: React.FC = () => {
  const [ froms, setFroms] = useState<FromResponse[]>([]);
  const [ tos, setTos] = useState<ToResponse[]>([]);
  const [ plans, setPlans] = useState<PlansResponse[]>([]);
  const [ cost, setCost] = useState<CostResponse>({} as CostResponse);

  const [ selectedFrom, setSelectedFrom] = useState<number>();
  const [ selectedTo, setSelectedTo] = useState<number>();
  const [ selectedPlain, setSelectedPlain] = useState<string>();
  const [ selectedCallTime, setSelectedCallTime] = useState<number>(0);

  useEffect(() => {
    api.get<FromResponse[]>('/charge-data/from').then(response => {
      const getFroms = response.data;

      setFroms(getFroms);
    })

    api.get<ToResponse[]>('/charge-data/to').then(response => {
      const getTos = response.data;

      setTos(getTos);
    })

    api.get<PlansResponse[]>('/plans').then(response => {
      const getPlans = response.data;

      setPlans(getPlans)
    })
  }, [])

  async function calculateCost(e: FormEvent) {
    e.preventDefault();

    const data = {
      dddFrom: selectedFrom,
      dddTo: selectedTo,
      callTime: selectedCallTime,
      plain: selectedPlain
    }

    const response = await api.post('/cost', data)

    setCost(response.data)
  }

  return (
    <div id="page-charge-consult" className="container">
      <Header title="Economize com o Fale Mais">
      <form action="" id="calculate-charges" onSubmit={calculateCost}>
        <div className="select-block">
          <label htmlFor="from">DDD de Origem</label>
          <select
            name="from"
            id="from"
            value={selectedFrom}
            onChange={(e) => {setSelectedFrom(Number(e.target.value))}}
          >
            <option value="0" hidden>Selecione uma opção</option>
            {froms.map(from => (
                <option key={from.dddFrom} value={from.dddFrom}>0{from.dddFrom}</option>
            ))}
          </select>
        </div>
        <div className="select-block">
          <label htmlFor="from">DDD de Destino</label>
          <select
            name="from"
            id="from"
            value={selectedTo}
            onChange={(e) => {setSelectedTo(Number(e.target.value))}}
          >
            <option value="0" hidden>Selecione uma opção</option>
            {tos.map(to => (
                <option key={to.dddTo+1} value={to.dddTo}>0{to.dddTo}</option>
            ))}
          </select>
        </div>
        <div className="select-block">
          <label htmlFor="from">Planos</label>
          <select
            name="from"
            id="from"
            value={selectedPlain}
            onChange={(e) => {setSelectedPlain(e.target.value)}}
          >
            <option value="0" hidden>Selecione uma opção</option>
            {plans.map(plain => (
                <option key={plain.id} value={plain.name}>{plain.name}</option>
            ))}
          </select>
        </div>
        <Input
          type="text"
          name="callTime"
          label="Tempo da chamada (minutos)"
          value={selectedCallTime}
          onChange={(e) => { setSelectedCallTime(Number(e.target.value))}}
        />
        <button type="submit">Buscar</button>
      </form>
      </Header>

      <main>
        <ChargeTable
            dddFrom={cost.from}
            dddTo={cost.to}
            plain={cost.plain}
            callTime={cost.time}
            withPlain={cost.withPlain}
            withoutPlain={cost.withoutPlain}
        />
      </main>

    </div>
  )
}
export default ChargeCalc;