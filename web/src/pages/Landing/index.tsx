import React from 'react';
import {Link} from 'react-router-dom';

import './styles.css';

import logo from '../../assets/images/logo.png';
import cartoon from '../../assets/images/cartoon.png';
import calculator from '../../assets/images/calculator.png';

const Landing: React.FC = () => {
  return (
    <div id="page-landing">
      <div id="page-landing-content" className="container">
        <div className="logo-container">
          <img src={logo} alt="FaleMais" className="logo-image"/>
          <h2>Calcule os gastos de sua chamada e veja o quanto você pode economizar usando nossa plataforma.</h2>
        </div>
        <img src={cartoon} alt="Calculo de gastos" className="cartoon-image"/>

        <div className="buttons-container">
          <Link to="/charge" className="charge">
            <img src={calculator} alt="Calculadora"/>
            Consultar
          </Link>
        </div>
      </div>
    </div>
  )
}
export default Landing;