<div align="center">
    <h2 align="center">Desafio Vizir - Backend</h3>
</div>

# Índice
- [Tecnologias Utilizadas](#tecnologias-utilizadas)
- [Documentação](#documentacao)
- [Sobre](#sobre)
- [Estrutura da Aplicação](#estrutura)
- [Iniciando a Aplicação](#executando)

<a id="tecnologias-utilizadas"></a>

## :wrench: Tecnologias Utilizadas

- [NodeJS](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
- [Express](https://expressjs.com/pt-br/)
- [TypeORM](https://typeorm.io/#/)
- [SQLite3](https://www.npmjs.com/package/sqlite)
- [Jest](https://jestjs.io/)
- [Babel](https://babeljs.io/)

<a id="sobre"></a>

## :scroll: Sobre

Essa aplicação tem como objetivo disponibilizar um CRUD completo para configuração de **Planos** e **Dados de cobrança**. Mas o principal é disponibilizar um endpoint que possibilite o calculo de gastos de uma chamada telefônica

Nesse caso o SQLite foi utilizado como banco de dados devido sua facilidade para a realização dos testes. Contudo, a alteração para utilização de um outro banco de dados é bem simples, basta atualizar o arquivo **ormconfig.js** com as configurações do novo banco

<a id="documentacao"></a>

## :books: Documentação
Para acessar a documentação do projeto, basta iniciar a aplicação e acessar o seguinte endpoint: http://ip-aplicação:3333/swagger. Você também pode encontrar a collection do postman na pasta raiz do projeto, como o nome: **DesafioVizirCollection.json**

<a id="estrutura"></a>

## :deciduous_tree: Estrutura da Aplicação

```
├── __tests__
│   ├── ChargeData
│   ├── CheckCostEndpoint.spec.ts
│   └── Plain
├── coverage
├── src
│   ├── config
│   ├── controllers
│   ├── database
│   │   ├── migrations
│   │   └── seeds
│   ├── models
│   ├── repositories
│   │   ├── ChargeData
│   │   └── Plans
│   ├── services
│   │   ├── ChargeData
│   │   └── Plans
│   │   ├── GetCallCostService.ts
│   ├── validators
│   └── web
│       ├── container
│       ├── dtos
│       ├── errors
│       ├── middlewares
│       ├── routes
│       ├── app.ts
│       └── server.ts
├── jest.config.js
├── ormconfig.js
├── package.json
├── prettier.config.js
├── README.MD
├── tsconfig.json
├── yarn-error.log
└── yarn.lock
```

<a id="executando"></a>

## :rocket: Iniciando a aplicação

- Acesse a pasta do projeto
- Na raiz do projeto execute o comando abaixo para instalação das dependências
```sh
  $ yarn install
```
- Execute os comandos abaixos para criação do banco de dados SQLite, para aplicar as migrations e para semear o banco de dados com os dados iniciais que foram informados no desafio
```sh
  # Aplicando as migrations
  $ yarn typeorm migration:run

  # Adicionando os items à tabela
  $ yarn typeorm seed:run
```
- Realize os testes de integração
```sh
  $ yarn test
```
>Ao finalizar os testes um relatório é gerado na pasta **coverage**. 
>
>Para vizualizar o mesmo acesse a pasta *coverage*, em seguida acesse a pasta *lcov-report*, nessa pasta existe um arquivo index.html, basta abri-lo no navegador que será possível verificar todo o relatório gerado a partir dos testes


- Inicie a aplicação em ambiente de desenvolvimento
```sh
  $ yarn dev:server
```

- Inicie a aplicação em ambiente de produção
```sh
  # Buildando a aplicação
  $ yarn build

  # Iniciando a aplicação 
  $ yarn prod:server
```

- Gerando imagem da aplicação com Dockerfile e iniciando container
```sh
  # Gerando imagem da aplicação
  # Executar o comando abaixo na pasta raiz do projeto
  $ docker build -t desafio-vizir .

  # Criando container
  $ docker run -d --env NODE_ENV=production -p3333:3333 --name desafio-vizir desafio-vizir
```