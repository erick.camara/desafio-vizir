import {injectable, inject} from 'tsyringe';

import ICreatePlansDTO from '../../web/dtos/ICreatePlansDTO';
import IResponsePlainDTO from '../../web/dtos/IResponsePlainDTO';
import IPlansRepository from '../../repositories/Plans/IPlansRepository';

@injectable()
class CreatePlain {
  constructor(
    @inject('PlansRepository')
    private plainsReporitory: IPlansRepository,
  ) {}

  public async execute(
    data: ICreatePlansDTO,
  ): Promise<IResponsePlainDTO | null> {
    const plain = await this.plainsReporitory.create(data);

    return plain || null;
  }
}
export default CreatePlain;
