import {injectable, inject} from 'tsyringe';

import IResponsePlainDTO from '../../web/dtos/IResponsePlainDTO';
import IPlansRepository from '../../repositories/Plans/IPlansRepository';

import {ERRORS} from '../../config/constants';
import AppError from '../../web/errors/AppError';

@injectable()
class GetAllPlans {
  constructor(
    @inject('PlansRepository')
    private plansReporitory: IPlansRepository,
  ) {}

  public async execute(): Promise<IResponsePlainDTO[] | null> {
    try {
      const plans = await this.plansReporitory.getAll();

      return plans || null;
    } catch (err) {
      console.error(err);
      throw new AppError(ERRORS.GET);
    }
  }
}
export default GetAllPlans;
