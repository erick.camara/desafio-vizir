import {injectable, inject} from 'tsyringe';

import IPlansRepository from '../../repositories/Plans/IPlansRepository';

@injectable()
class DeletePlain {
  constructor(
    @inject('PlansRepository')
    private plansReporitory: IPlansRepository,
  ) {}

  public async execute(id: number): Promise<string | null> {
    const deletedPlain = await this.plansReporitory.delete(id);

    return deletedPlain || null;
  }
}
export default DeletePlain;
