import {injectable, inject} from 'tsyringe';

import IResponsePlainDTO from '../../web/dtos/IResponsePlainDTO';
import IPlansRepository from '../../repositories/Plans/IPlansRepository';

@injectable()
class FindById {
  constructor(
    @inject('PlansRepository')
    private plansReporitory: IPlansRepository,
  ) {}

  public async execute(id: number): Promise<IResponsePlainDTO | null> {
    const plans = await this.plansReporitory.findById(id);

    return plans || null;
  }
}
export default FindById;
