import {injectable, inject} from 'tsyringe';
import {UpdateResult} from 'typeorm';

import ICreatePlansDTO from '../../web/dtos/ICreatePlansDTO';
import IPlansRepository from '../../repositories/Plans/IPlansRepository';

import {ERRORS} from '../../config/constants';
import AppError from '../../web/errors/AppError';

@injectable()
class UpdatePlans {
  constructor(
    @inject('PlansRepository')
    private plansReporitory: IPlansRepository,
  ) {}

  public async execute(
    data: ICreatePlansDTO,
    id: number,
  ): Promise<UpdateResult | null> {
    try {
      const {name, time} = data;

      if (!name || !time) {
        throw new AppError(ERRORS.UPDATE);
      }
      const plans = await this.plansReporitory.update(data, id);

      return plans || null;
    } catch (err) {
      console.error(err);
      throw new AppError(ERRORS.UPDATE);
    }
  }
}
export default UpdatePlans;
