import {injectable, inject} from 'tsyringe';

import IResponseChargeDataDTO from '../../web/dtos/IResponseChargeDataDTO';
import IChargeDataRepository from '../../repositories/ChargeData/IChargeDataRepository';

@injectable()
class GetAllChargeData {
  constructor(
    @inject('ChargeDataRepository')
    private chargeDataReporitory: IChargeDataRepository,
  ) {}

  public async execute(): Promise<IResponseChargeDataDTO[] | null> {
    const chargeData = await this.chargeDataReporitory.getAll();

    return chargeData || null;
  }
}
export default GetAllChargeData;
