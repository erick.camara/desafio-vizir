import {injectable, inject} from 'tsyringe';

import IChargeDataRepository from '../../repositories/ChargeData/IChargeDataRepository';

@injectable()
class DeleteChargeData {
  constructor(
    @inject('ChargeDataRepository')
    private chargeDataReporitory: IChargeDataRepository,
  ) {}

  public async execute(id: number): Promise<string | null> {
    const chargeData = await this.chargeDataReporitory.delete(id);

    return chargeData || null;
  }
}
export default DeleteChargeData;
