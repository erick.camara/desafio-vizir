import {injectable, inject} from 'tsyringe';

import IChargeDataRepository from '../../repositories/ChargeData/IChargeDataRepository';

@injectable()
class GetDddChargeData {
  constructor(
    @inject('ChargeDataRepository')
    private chargeDataReporitory: IChargeDataRepository,
  ) {}

  public async getFrom(): Promise<number[] | null> {
    const from = await this.chargeDataReporitory.getDddFrom()

    return from || null;
  }

  public async getTo(): Promise<number[] | null> {
    const to = await this.chargeDataReporitory.getDddTo()

    return to || null;
  }
}
export default GetDddChargeData;
