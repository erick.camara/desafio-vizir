import {injectable, inject} from 'tsyringe';

import ICreateChargeDTO from '../../web/dtos/ICreateChargeDataDTO';
import IResponseChargeDataDTO from '../../web/dtos/IResponseChargeDataDTO';
import IChargeDataRepository from '../../repositories/ChargeData/IChargeDataRepository';

@injectable()
class CreateChargeData {
  constructor(
    @inject('ChargeDataRepository')
    private chargeDataReporitory: IChargeDataRepository,
  ) {}

  public async execute(
    data: ICreateChargeDTO,
  ): Promise<IResponseChargeDataDTO | null> {
    const chargeData = await this.chargeDataReporitory.create(data);

    return chargeData || null;
  }
}
export default CreateChargeData;
