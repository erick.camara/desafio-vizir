import {injectable, inject} from 'tsyringe';

import IResponseChargeDataDTO from '../../web/dtos/IResponseChargeDataDTO';
import IChargeDataRepository from '../../repositories/ChargeData/IChargeDataRepository';

@injectable()
class FindById {
  constructor(
    @inject('ChargeDataRepository')
    private chargeDataReporitory: IChargeDataRepository,
  ) {}

  public async execute(id: number): Promise<IResponseChargeDataDTO | null> {
    const chargeData = await this.chargeDataReporitory.findById(id);

    return chargeData || null;
  }
}
export default FindById;
