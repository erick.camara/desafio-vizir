import {injectable, inject} from 'tsyringe';
import {UpdateResult} from 'typeorm';

import ICreateChargeDTO from '../../web/dtos/ICreateChargeDataDTO';
import IChargeDataRepository from '../../repositories/ChargeData/IChargeDataRepository';

import AppError from '../../web/errors/AppError';
import {ERRORS} from '../../config/constants';

@injectable()
class UpdateChargeData {
  constructor(
    @inject('ChargeDataRepository')
    private chargeDataReporitory: IChargeDataRepository,
  ) {}

  public async execute(
    data: ICreateChargeDTO,
    id: number,
  ): Promise<UpdateResult | null> {
    try {
      const {dddFrom, dddTo, cost} = data;

      if (!dddFrom || !dddTo || !cost) {
        throw new AppError(ERRORS.UPDATE);
      }
      const chargeData = await this.chargeDataReporitory.update(data, id);

      return chargeData || null;
    } catch (err) {
      console.error(err);
      throw new AppError(ERRORS.UPDATE);
    }
  }
}
export default UpdateChargeData;
