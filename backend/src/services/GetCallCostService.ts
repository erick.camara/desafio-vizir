import {injectable, inject} from 'tsyringe';

import IChargeDataDTO from '../web/dtos/IChargeDataDTO';
import IResponseCostDTO from '../web/dtos/IResponseCostDTO';
import IPlansRepository from '../repositories/Plans/IPlansRepository';
import IChargeDataRepository from '../repositories/ChargeData/IChargeDataRepository';

import {ERRORS} from '../config/constants';
import AppError from '../web/errors/AppError';

@injectable()
class GetCallCostService {
  constructor(
    @inject('ChargeDataRepository')
    private chargeDataReporitory: IChargeDataRepository,

    @inject('PlansRepository')
    private plansRepository: IPlansRepository,
  ) {}

  public async execute(data: IChargeDataDTO): Promise<IResponseCostDTO | null> {
    const {dddFrom, dddTo, callTime, plain} = data;

    try {
      const getCallCost = await this.chargeDataReporitory.findCallCost(
        dddFrom,
        dddTo,
      );

      const getPlain = await this.plansRepository.findByName(plain);

      if (getCallCost?.cost && getPlain?.time) {
        const cost = getCallCost?.cost as number;
        const time = getPlain?.time as number;

        let overTime = callTime - time;

        if (overTime < 0) {
          overTime = 0;
        }
        const plainPrice = (cost + 0.1 * cost) * overTime;
        const priceWithoutPlain = cost * callTime;

        const mountResponse = {
          from: dddFrom,
          to: dddTo,
          time: callTime,
          plain,
          withPlain: plainPrice,
          withoutPlain: priceWithoutPlain,
        };

        return mountResponse;
      }

      const mountResponse = {
        from: dddFrom,
        to: dddTo,
        time: callTime,
        plain,
        withPlain: '-',
        withoutPlain: '-',
      };

      return mountResponse;
    } catch (err) {
      console.error(err);
      throw new AppError(ERRORS.COST);
    }
  }
}
export default GetCallCostService;
