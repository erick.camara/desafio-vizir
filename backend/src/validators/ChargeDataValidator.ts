import {Request, Response, NextFunction} from 'express';
import Joi from 'joi';

import AppError from '../web/errors/AppError';
import {VALIDATOR, ERRORS} from '../config/constants';

class ChargeDataValidator {
  async createValidator(
    request: Request,
    response: Response,
    next: NextFunction,
  ) {
    try {
      const data = Joi.object().keys({
        dddFrom: Joi.number().required().label(VALIDATOR.REQUIRED.FROM),
        dddTo: Joi.number().required().label(VALIDATOR.REQUIRED.TO),
        cost: Joi.number().required().label(VALIDATOR.REQUIRED.COST),
      });
      const validateData = data.validate(request.body, {abortEarly: false});
      if (!validateData.error) {
        return next();
      }

      const errors = validateData.error.details.map(
        (error) => error.context?.label,
      );
      return response
        .status(400)
        .json({error: 'Error in Validation', messages: errors});
    } catch (err) {
      console.error(err);
      throw new AppError(ERRORS.VALIDATOR);
    }
  }

  async updateValidator(
    request: Request,
    response: Response,
    next: NextFunction,
  ) {
    try {
      const data = Joi.object().keys({
        dddFrom: Joi.number().required().label(VALIDATOR.REQUIRED.FROM),
        dddTo: Joi.number().required().label(VALIDATOR.REQUIRED.TO),
        cost: Joi.number().required().label(VALIDATOR.REQUIRED.COST),
      });
      const validateData = data.validate(request.body, {abortEarly: false});
      if (!validateData.error) {
        return next();
      }

      const errors = validateData.error.details.map(
        (error) => error.context?.label,
      );
      return response
        .status(400)
        .json({error: 'Error in Validation', messages: errors});
    } catch (err) {
      console.error(err);
      throw new AppError(ERRORS.VALIDATOR);
    }
  }

  async idValidator(request: Request, response: Response, next: NextFunction) {
    try {
      if (!request.params.id) {
        return response.status(400).json({error: 'Validation fails'});
      }
      return next();
    } catch (err) {
      console.error(err);
      throw new AppError(ERRORS.VALIDATOR);
    }
  }
}
export default new ChargeDataValidator();
