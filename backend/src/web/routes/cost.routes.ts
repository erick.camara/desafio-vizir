import {Router} from 'express';
import {container} from 'tsyringe';

import CostController from '../../controllers/CostController';
import CostValidator from '../../validators/CostValidator';

const costRouter = Router();
const costController = container.resolve(CostController);

costRouter.post('/', CostValidator.createValidator, costController.index);

export default costRouter;
