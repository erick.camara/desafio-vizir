import {Router} from 'express';

import chargeRouter from './chargeData.routes';
import plansRouter from './plans.routes';
import costRouter from './cost.routes';

const baseUrl = '/api/v1';
const routes = Router();

routes.use(`${baseUrl}/cost`, costRouter);
routes.use(`${baseUrl}/charge-data`, chargeRouter);
routes.use(`${baseUrl}/plans`, plansRouter);

export default routes;
