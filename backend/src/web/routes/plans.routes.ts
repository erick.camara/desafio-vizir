import {Router} from 'express';
import {container} from 'tsyringe';

import PlansController from '../../controllers/PlansController';
import PlansValidator from '../../validators/PlansValidator';

const plansRouter = Router();
const plansController = container.resolve(PlansController);

plansRouter.get('/', plansController.index);
plansRouter.get('/:id', PlansValidator.idValidator, plansController.indexOne);
plansRouter.post('/', PlansValidator.createValidator, plansController.create);
plansRouter.delete('/:id', PlansValidator.idValidator, plansController.delete);
plansRouter.put('/:id', PlansValidator.updateValidator, plansController.update);

export default plansRouter;
