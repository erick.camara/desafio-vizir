/* eslint-disable prettier/prettier */
import {Router} from 'express';
import {container} from 'tsyringe';

import ChargeDataController from '../../controllers/ChargeDataController';
import ChargeDataValidator from '../../validators/ChargeDataValidator';

const chargeRouter = Router();
const chargeDataController = container.resolve(ChargeDataController);

chargeRouter.get('/', chargeDataController.index);
chargeRouter.get('/from', chargeDataController.indexFrom);
chargeRouter.get('/to', chargeDataController.indexTo);
chargeRouter.get('/:id', ChargeDataValidator.idValidator, chargeDataController.indexOne);
chargeRouter.post('/', ChargeDataValidator.createValidator, chargeDataController.create);
chargeRouter.delete('/:id', ChargeDataValidator.idValidator, chargeDataController.delete);
chargeRouter.put('/:id', ChargeDataValidator.updateValidator, chargeDataController.update);

export default chargeRouter;
