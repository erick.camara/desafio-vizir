import 'reflect-metadata';
import cors from "cors"

import './container';
import '../database';

import express from 'express';
import 'express-async-errors';
import swaggerUi from 'swagger-ui-express';
import swaggerConfig from '../config/swagger.json';

import routes from './routes';

import handleErrors from './middlewares/handleErrors';

const app = express();

app.use(cors());
app.use(express.json());
app.use(routes);
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerConfig));
app.use(handleErrors);

export default app;
