export default interface IChargeDataDTO {
  dddFrom: number;

  dddTo: number;

  callTime: number;

  plain: string;
}
