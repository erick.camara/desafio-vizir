export default interface IResponsePlainDTO {
  id: number;

  name: string;

  time: number;

  created_at: Date;

  updated_at: Date;
}
