export default interface ICreateChargeDataDTO {
  dddFrom: number;

  dddTo: number;

  cost: number;
}
