export default interface IResponseCostDTO {
  from: number;

  to: number;

  time: number;

  plain: string;

  withPlain: number | string;

  withoutPlain: number | string;
}
