export default interface IResponseChargeDataDTO {
  id: number;

  dddFrom: number;

  dddTo: number;

  cost: number;

  created_at: Date;

  updated_at: Date;
}
