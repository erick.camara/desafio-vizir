import {container} from 'tsyringe';

import PlansRepository from '../../repositories/Plans/PlansRepository';
import IPlansRepository from '../../repositories/Plans/IPlansRepository';

import ChargeDataRepository from '../../repositories/ChargeData/ChargeDataRepository';
import IChargeDataRepository from '../../repositories/ChargeData/IChargeDataRepository';

container.registerSingleton<IPlansRepository>(
  'PlansRepository',
  PlansRepository,
);

container.registerSingleton<IChargeDataRepository>(
  'ChargeDataRepository',
  ChargeDataRepository,
);
