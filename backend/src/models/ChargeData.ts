import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('charge_data')
class ChargeData {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({name: 'ddd_from'})
  dddFrom: number;

  @Column({name: 'ddd_to'})
  dddTo: number;

  @Column()
  cost: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
export default ChargeData;
