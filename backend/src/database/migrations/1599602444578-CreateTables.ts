import {MigrationInterface, QueryRunner} from 'typeorm';

export default class CreateTables1599602444578 implements MigrationInterface {
  name = 'CreateTables1599602444578';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "charge_data" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "ddd_from" integer NOT NULL, "ddd_to" integer NOT NULL, "cost" integer NOT NULL, "created_at" datetime NOT NULL DEFAULT (datetime('now')), "updated_at" datetime NOT NULL DEFAULT (datetime('now')))`,
    );
    await queryRunner.query(
      `CREATE TABLE "plans" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "time" integer NOT NULL, "created_at" datetime NOT NULL DEFAULT (datetime('now')), "updated_at" datetime NOT NULL DEFAULT (datetime('now')))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "plans"`);
    await queryRunner.query(`DROP TABLE "charge_data"`);
  }
}
