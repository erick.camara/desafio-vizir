import {Factory, Seeder} from 'typeorm-seeding';
import {Connection} from 'typeorm';
import Plans from '../../models/Plans';

class SeedItems implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<void> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(Plans)
      .values([
        {name: 'faleMais30', time: 30},
        {name: 'faleMais60', time: 60},
        {name: 'faleMais120', time: 120},
      ])
      .execute();
  }
}
export default SeedItems;
