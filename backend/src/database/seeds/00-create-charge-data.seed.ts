import {Factory, Seeder} from 'typeorm-seeding';
import {Connection} from 'typeorm';
import ChargeData from '../../models/ChargeData';

class SeedItems implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<void> {
    await connection
      .createQueryBuilder()
      .insert()
      .into(ChargeData)
      .values([
        {dddFrom: 11, dddTo: 16, cost: 1.9},
        {dddFrom: 16, dddTo: 11, cost: 2.9},
        {dddFrom: 11, dddTo: 17, cost: 1.7},
        {dddFrom: 17, dddTo: 11, cost: 2.7},
        {dddFrom: 11, dddTo: 18, cost: 0.9},
        {dddFrom: 18, dddTo: 11, cost: 1.9},
      ])
      .execute();
  }
}
export default SeedItems;
