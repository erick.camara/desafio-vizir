/* eslint-disable import/prefer-default-export */

export const ERRORS = {
  COST: 'Could not get cost of that call. Something went wrong',
  DELETE: 'Could not delete data. Something went wrong',
  UPDATE: 'Could not update data. Something went wrong',
  VALIDATOR: 'Could not validate data. Something went wrong',
  GET: 'Could not get information. Something went wrong',
};

export const MESSAGES = {
  CHARGE: {
    DELETED: 'Charge Data has been successfully deleted',
  },
  PLAIN: {
    DELETED: 'Plain has been successfully deleted',
  },
};

export const VALIDATOR = {
  REQUIRED: {
    NAME: 'Param name is required',
    TIME: 'Param time is required',

    FROM: 'Param dddFrom is required',
    TO: 'Param dddTo is required',
    COST: 'Param cost is required',
    CALLTIME: 'Param callTime is required',
    PLAIN: 'Param plain is required',
    MAXVALUE: 'Max value should be 2 characters',
  },
};
