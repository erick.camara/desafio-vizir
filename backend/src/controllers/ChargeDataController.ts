import {Request, Response} from 'express';
import {container} from 'tsyringe';

import FindById from '../services/ChargeData/FindById';
import GetAllChargeData from '../services/ChargeData/GetAllChargeData';
import GetDddChargeData from '../services/ChargeData/GetDddChargeData';
import CreateChargeData from '../services/ChargeData/CreateChargeData';
import DeleteChargeData from '../services/ChargeData/DeleteChargeData';
import UpdateChargeData from '../services/ChargeData/UpdateChargeData';

class ChargeDataController {
  public async index(request: Request, response: Response): Promise<Response> {
    const getChargeData = container.resolve(GetAllChargeData);

    const res = await getChargeData.execute();

    return response.json(res);
  }

  public async indexFrom(request: Request, response: Response): Promise<Response> {
    const getFromChargeData = container.resolve(GetDddChargeData);

    const res = await getFromChargeData.getFrom();

    return response.json(res);
  }

  public async indexTo(request: Request, response: Response): Promise<Response> {
    const getToChargeData = container.resolve(GetDddChargeData);

    const res = await getToChargeData.getTo();

    return response.json(res);
  }

  public async indexOne(
    request: Request,
    response: Response,
  ): Promise<Response> {
    const getChargeData = container.resolve(FindById);

    const res = await getChargeData.execute(Number(request.params.id));

    return response.json(res);
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const createChargeData = container.resolve(CreateChargeData);

    const res = await createChargeData.execute(request.body);

    return response.json(res);
  }

  public async delete(request: Request, response: Response): Promise<Response> {
    const deleteChargeData = container.resolve(DeleteChargeData);

    const res = await deleteChargeData.execute(Number(request.params.id));

    return response.json({data: res});
  }

  public async update(request: Request, response: Response): Promise<Response> {
    const updateChargeData = container.resolve(UpdateChargeData);

    const res = await updateChargeData.execute(
      request.body,
      Number(request.params.id),
    );

    return response.json(res);
  }
}
export default ChargeDataController;
