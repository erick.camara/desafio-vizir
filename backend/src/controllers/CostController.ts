import {Request, Response} from 'express';
import {container} from 'tsyringe';

import GetCallCostService from '../services/GetCallCostService';

class ChargeDataController {
  public async index(request: Request, response: Response): Promise<Response> {
    const calcCost = container.resolve(GetCallCostService);

    const res = await calcCost.execute(request.body);

    return response.json(res);
  }
}
export default ChargeDataController;
