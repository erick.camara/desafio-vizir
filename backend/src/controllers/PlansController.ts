import {Request, Response} from 'express';
import {container} from 'tsyringe';

import FindById from '../services/Plans/FindById';
import GetAllPlans from '../services/Plans/GetAllPlans';
import CreatePlain from '../services/Plans/CreatePlain';
import DeletePlain from '../services/Plans/DeletePlain';
import UpdatePlans from '../services/Plans/UpdatePlans';

class PlansController {
  public async index(request: Request, response: Response): Promise<Response> {
    const getPlans = container.resolve(GetAllPlans);

    const res = await getPlans.execute();

    return response.json(res);
  }

  public async indexOne(
    request: Request,
    response: Response,
  ): Promise<Response> {
    const findPlain = container.resolve(FindById);

    const res = await findPlain.execute(Number(request.params.id));

    return response.json(res);
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const createPlain = container.resolve(CreatePlain);

    const res = await createPlain.execute(request.body);

    return response.json(res);
  }

  public async delete(request: Request, response: Response): Promise<Response> {
    const deletePlain = container.resolve(DeletePlain);

    const res = await deletePlain.execute(Number(request.params.id));

    return response.json({data: res});
  }

  public async update(request: Request, response: Response): Promise<Response> {
    const updatePlain = container.resolve(UpdatePlans);

    const res = await updatePlain.execute(
      request.body,
      Number(request.params.id),
    );

    return response.json(res);
  }
}
export default PlansController;
