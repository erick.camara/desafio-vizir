import {
  getRepository,
  EntityRepository,
  Repository,
  UpdateResult,
} from 'typeorm';

import Plans from '../../models/Plans';
import IPlansRepository from './IPlansRepository';
import ICreatePlansDTO from '../../web/dtos/ICreatePlansDTO';

import AppError from '../../web/errors/AppError';
import {ERRORS, MESSAGES} from '../../config/constants';

@EntityRepository(Plans)
class PlansRepository implements IPlansRepository {
  private ormRepository: Repository<Plans>;

  constructor() {
    this.ormRepository = getRepository(Plans);
  }

  public async findByName(name: string): Promise<Plans | null> {
    const findPlain = await this.ormRepository.findOne({
      where: {name},
    });

    return findPlain || null;
  }

  public async findById(id: number): Promise<Plans | null> {
    const findPlain = await this.ormRepository.findOne({
      where: {id},
    });

    return findPlain || null;
  }

  public async getAll(): Promise<Plans[] | null> {
    const findAll = await this.ormRepository.find();

    return findAll;
  }

  public async create({name, time}: ICreatePlansDTO): Promise<Plans> {
    const createPlans = this.ormRepository.create({name, time});

    await this.ormRepository.save(createPlans);

    return createPlans || null;
  }

  public async delete(id: number): Promise<string> {
    try {
      await this.ormRepository.delete(id);

      return MESSAGES.PLAIN.DELETED;
    } catch (err) {
      return ERRORS.DELETE;
    }
  }

  public async update(
    {name, time}: ICreatePlansDTO,
    id: number,
  ): Promise<UpdateResult> {
    try {
      const updatePlans = await this.ormRepository
        .createQueryBuilder()
        .update(Plans)
        .set({name, time})
        .where({id})
        .execute();

      return updatePlans;
    } catch (err) {
      console.error(err);
      throw new AppError(ERRORS.UPDATE);
    }
  }
}

export default PlansRepository;
