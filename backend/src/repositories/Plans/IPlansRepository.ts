import {UpdateResult} from 'typeorm';

import Plans from '../../models/Plans';
import ICreatePlansDTO from '../../web/dtos/ICreatePlansDTO';

export default interface IPlansRepository {
  findByName(name: string): Promise<Plans | null>;
  findById(id: number): Promise<Plans | null>;
  getAll(): Promise<Plans[] | null>;
  create(data: ICreatePlansDTO): Promise<Plans | null>;
  delete(id: number): Promise<string>;
  update(data: ICreatePlansDTO, id: number): Promise<UpdateResult>;
}
