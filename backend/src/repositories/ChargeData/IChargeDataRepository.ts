import {UpdateResult} from 'typeorm';
import ChargeData from '../../models/ChargeData';
import ICreateChargeDataDTO from '../../web/dtos/ICreateChargeDataDTO';

export default interface IChargeDataRepository {
  findCallCost(dddFrom: number, dddTo: number): Promise<ChargeData | null>;
  findById(id: number): Promise<ChargeData | null>;
  getAll(): Promise<ChargeData[] | null>;
  getDddFrom(): Promise<number[] | null>;
  getDddTo(): Promise<number[] | null>;
  create(data: ICreateChargeDataDTO): Promise<ChargeData | null>;
  delete(id: number): Promise<string>;
  update(data: ICreateChargeDataDTO, id: number): Promise<UpdateResult>;
}
