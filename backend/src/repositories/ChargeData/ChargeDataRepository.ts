import {
  getRepository,
  EntityRepository,
  Repository,
  UpdateResult,
} from 'typeorm';

import ChargeData from '../../models/ChargeData';
import IChargeDataRepository from './IChargeDataRepository';
import ICreateChargeDTO from '../../web/dtos/ICreateChargeDataDTO';

import AppError from '../../web/errors/AppError';
import {MESSAGES, ERRORS} from '../../config/constants';

@EntityRepository(ChargeData)
class ChargeDataRepository implements IChargeDataRepository {
  private ormRepository: Repository<ChargeData>;

  constructor() {
    this.ormRepository = getRepository(ChargeData);
  }

  public async findCallCost(
    dddFrom: number,
    dddTo: number,
  ): Promise<ChargeData | null> {
    const findCost = await this.ormRepository.findOne({
      where: {dddFrom, dddTo},
    });

    return findCost || null;
  }

  public async findById(id: number): Promise<ChargeData | null> {
    const findPlain = await this.ormRepository.findOne({
      where: {id},
    });

    return findPlain || null;
  }

  public async getAll(): Promise<ChargeData[] | null> {
    const findAll = await this.ormRepository.find();

    return findAll;
  }

  public async getDddFrom(): Promise<number[] | null> {
    const findFromDdd = await this.ormRepository.createQueryBuilder()
      .select('ddd_from as dddFrom')
      .groupBy('ddd_from')
      .having('ddd_from > :num', {num: 1})
      .execute()

    return findFromDdd;
  }

  public async getDddTo(): Promise<number[] | null> {
    const findToDdd = await this.ormRepository.createQueryBuilder()
      .select('ddd_to as dddTo')
      .groupBy('ddd_to')
      .having('ddd_to > :num', {num: 1})
      .execute()

    return findToDdd;
  }

  public async create({
    dddFrom,
    dddTo,
    cost,
  }: ICreateChargeDTO): Promise<ChargeData> {
    const createChargeData = this.ormRepository.create({
      dddFrom,
      dddTo,
      cost,
    });

    await this.ormRepository.save(createChargeData);

    return createChargeData || null;
  }

  public async delete(id: number): Promise<string> {
    try {
      await this.ormRepository.delete(id);

      return MESSAGES.CHARGE.DELETED;
    } catch (err) {
      return ERRORS.DELETE;
    }
  }

  public async update(
    {dddFrom, dddTo, cost}: ICreateChargeDTO,
    id: number,
  ): Promise<UpdateResult> {
    try {
      const updateChargeData = await this.ormRepository
        .createQueryBuilder()
        .update(ChargeData)
        .set({dddFrom, dddTo, cost})
        .where({id})
        .execute();

      return updateChargeData;
    } catch (err) {
      console.error(err);
      throw new AppError(ERRORS.UPDATE);
    }
  }
}

export default ChargeDataRepository;
