const dev = {
  type: 'sqlite',
  database: './src/database/database.sqlite',
  entities: ['./src/models/*.ts'],
  migrations: ['./src/database/migrations/*.ts'],
  cli: {
    migrationsDir: './src/database/migrations',
  },
  seeds: ['src/database/seeds/*{.ts,.js}'],
};
const prod = {
  type: 'sqlite',
  database: './dist/database/database.sqlite',
  entities: ['./dist/models/*.js'],
  migrations: ['./dist/database/migrations/*.js'],
  cli: {
    migrationsDir: './dist/database/migrations',
  },
  seeds: ['dist/database/seeds/*{.ts,.js}'],
};
module.exports = process.env.NODE_ENV == 'production' ? prod : dev;