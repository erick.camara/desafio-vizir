import supertest from 'supertest';

import app from '../../src/web/app';

jest.mock('../../src/repositories/Plans/PlansRepository');

const request = supertest(app);

describe('CheckUpdatePlainEndpoint', () => {
  it('should be able to update a specific plain', async () => {
    const response = await request
      .put('/api/v1/plans/1')
      .set('Accept', 'application/json')
      .send({name: 'faleMais160', time: 160});

    expect(response.status).toBe(200);
  });
});
