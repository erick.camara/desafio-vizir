import supertest from 'supertest';

import app from '../../src/web/app';

jest.mock('../../src/repositories/Plans/PlansRepository');

const request = supertest(app);

describe('CheckCreatePlainEndpoint', () => {
  it('should be able to create a new plain', async () => {
    const response = await request
      .post('/api/v1/plans')
      .set('Accept', 'application/json')
      .send({name: 'faleMais160', time: 160});
    // .expect(function (res) {
    //   res.body.name = 'faleMais160';
    //   res.body.time = 160;
    // });

    expect(response.status).toBe(200);
    // expect(response.body).toBeCalledWith(
    //   expect.objectContaining({
    //     id: expect.any(Number),
    //     name: expect.any(String),
    //     time: expect.any(Number),
    //     created_at: expect.any(Date),
    //     updated_at: expect.any(Date),
    //   }),
    // );
  });
});
