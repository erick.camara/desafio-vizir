import supertest from 'supertest';

import app from '../../src/web/app';

jest.mock('../../src/repositories/Plans/PlansRepository');

const request = supertest(app);

describe('CheckGetPlansEndpoint', () => {
  it('should be able to get all plans', async () => {
    const response = await request.get('/api/v1/plans');

    expect(response.status).toBe(200);
  });
});
