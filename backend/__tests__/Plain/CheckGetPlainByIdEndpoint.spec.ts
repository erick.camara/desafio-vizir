import supertest from 'supertest';

import app from '../../src/web/app';

jest.mock('../../src/repositories/Plans/PlansRepository');

const request = supertest(app);

describe('CheckGetPlainByIdEndpoint', () => {
  it('should be able to get a specific charge data', async () => {
    const response = await request.get('/api/v1/plans/1');

    expect(response.status).toBe(200);
  });
});
