import supertest from 'supertest';

import app from '../../src/web/app';

jest.mock('../../src/repositories/Plans/PlansRepository');

const request = supertest(app);

describe('CheckDeletePlainEndpoint', () => {
  it('should be able to delete a specific plain', async () => {
    const response = await request.delete('/api/v1/plans/1');

    expect(response.status).toBe(200);
  });
});
