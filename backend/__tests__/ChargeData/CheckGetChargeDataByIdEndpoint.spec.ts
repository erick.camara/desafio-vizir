import supertest from 'supertest';

import app from '../../src/web/app';

jest.mock('../../src/repositories/ChargeData/ChargeDataRepository');

const request = supertest(app);

describe('CheckGetChargeDataByIdEndpoint', () => {
  it('should be able to get a specific charge data', async () => {
    const response = await request.get('/api/v1/charge-data/1');

    expect(response.status).toBe(200);
  });
});
