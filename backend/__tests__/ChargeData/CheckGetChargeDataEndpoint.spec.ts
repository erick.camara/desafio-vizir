import supertest from 'supertest';

import app from '../../src/web/app';

jest.mock('../../src/repositories/ChargeData/ChargeDataRepository');

const request = supertest(app);

describe('CheckGetChargeDataEndpoint', () => {
  it('should be able to get all charge data', async () => {
    const response = await request.get('/api/v1/charge-data');

    expect(response.status).toBe(200);
  });
});
