import supertest from 'supertest';

import app from '../../src/web/app';

jest.mock('../../src/repositories/ChargeData/ChargeDataRepository');

const request = supertest(app);

describe('CheckCreateChargeDataEndpoint', () => {
  it('should be able to create a new charge data', async () => {
    const response = await request
      .post('/api/v1/charge-data')
      .set('Accept', 'application/json')
      .send({
        dddFrom: 18,
        dddTo: 17,
        cost: 1.7,
      });

    expect(response.status).toBe(200);
  });
});
