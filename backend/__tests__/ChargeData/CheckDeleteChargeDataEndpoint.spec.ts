import supertest from 'supertest';

import app from '../../src/web/app';

jest.mock('../../src/repositories/ChargeData/ChargeDataRepository');

const request = supertest(app);

describe('CheckDeleteChargeDataEndpoint', () => {
  it('should be able to delete a specific charge data', async () => {
    const response = await request.delete('/api/v1/charge-data/1');

    expect(response.status).toBe(200);
  });
});
