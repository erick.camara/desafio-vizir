import supertest from 'supertest';

import app from '../../src/web/app';

jest.mock('../../src/repositories/ChargeData/ChargeDataRepository');

const request = supertest(app);

describe('CheckUpdateChargeDataEndpoint', () => {
  it('should be able to update charge data information by id', async () => {
    const response = await request
      .put('/api/v1/charge-data/1')
      .set('Accept', 'application/json')
      .send({
        dddFrom: 18,
        dddTo: 17,
        cost: 1.7,
      });

    expect(response.status).toBe(200);
  });
});
