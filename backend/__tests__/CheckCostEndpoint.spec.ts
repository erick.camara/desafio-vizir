import supertest from 'supertest';

import app from '../src/web/app';

jest.mock('../src/repositories/Plans/PlansRepository');
jest.mock('../src/repositories/ChargeData/ChargeDataRepository');

const request = supertest(app);

describe('CheckCostEndpoint', () => {
  it('should be able to calculate and return cost information about a call', async () => {
    const response = await request
      .post('/api/v1/cost')
      .set('Accept', 'application/json')
      .send({
        dddFrom: 11,
        dddTo: 17,
        callTime: 80,
        plain: 'faleMais60',
      });

    expect(response.status).toBe(200);
  });
});
